package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebElement;
import org.testng.annotations.Test;

import base.CommonClass;

public class Phase1 extends CommonClass{
	
	
	
	@Test
	public void runPhase1() throws InterruptedException {
		driver.findElement(By.id("action-button")).click();
		Thread.sleep(10000);
		WebElement eleSearchBox = driver.findElementByXPath("//input[contains(@title,'Search')]");
		
		eleSearchBox.sendKeys("naveen newbee",Keys.ENTER);
		WebElement eleTypeBox = driver.findElementByXPath("//div[text()='Type a message']//following-sibling::div");
		
		eleTypeBox.sendKeys("Hey Naveen!! Test Message",Keys.ENTER);
		driver.findElementByXPath("//span[@data-icon='clip']").click();
		LocalFileDetector detector = new LocalFileDetector();
		WebElement eleUpload = driver.findElementByXPath("(//input[@type='file'])[1]");
		((RemoteWebElement)eleUpload).setFileDetector(detector);
		eleUpload
		.sendKeys(detector.getLocalFile("./Images/img.png")
		.getAbsolutePath()); 
		driver.findElementByXPath("(//div[contains(@class,'_3u328')])[1]").sendKeys("Kind attention - Due to some medical issues Babu is unable to conduct Webservices webinar scheduled today @9 pm."
				+"This webinar will soon be scheduled next week and will be updated to you all. Regret for any inconveniences caused."
				+"Thanks Testleaf");
		WebElement eleSendButton = driver.findElementByXPath("//span[@data-icon='send-light']");
		eleSendButton.click();
	
	}

}
